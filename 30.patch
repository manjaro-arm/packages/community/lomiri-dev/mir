From 52d1f1c6db453c04c941e8b71e0011c3c6ddb422 Mon Sep 17 00:00:00 2001
From: Alan Griffiths <alan@octopull.co.uk>
Date: Wed, 2 Sep 2020 12:45:33 +0100
Subject: [PATCH 1/2] Compositing shouldn't block in eglSwapBuffers()

---
 src/platforms/wayland/displayclient.cpp | 5 +++++
 1 file changed, 5 insertions(+)

diff --git a/src/platforms/wayland/displayclient.cpp b/src/platforms/wayland/displayclient.cpp
index 13f0c31da3..cbf4f980fb 100644
--- a/src/platforms/wayland/displayclient.cpp
+++ b/src/platforms/wayland/displayclient.cpp
@@ -342,6 +342,11 @@ void mgw::DisplayClient::Output::release_current()
 
 void mgw::DisplayClient::Output::swap_buffers()
 {
+    // Avoid throttling compositing by blocking in eglSwapBuffers().
+    // NB We can likely do even better by adding a frame callback
+    // and using that to schedule composition
+    eglSwapInterval(owner->egldisplay, 0);
+
     if (eglSwapBuffers(owner->egldisplay, eglsurface) != EGL_TRUE)
         BOOST_THROW_EXCEPTION(egl_error("Failed to perform buffer swap"));
 }

From 55c2b65272633fb8463d10890533e986c239b65f Mon Sep 17 00:00:00 2001
From: Alan Griffiths <alan@octopull.co.uk>
Date: Wed, 2 Sep 2020 15:47:39 +0100
Subject: [PATCH 2/2] Add frame callback

---
 src/platforms/wayland/displayclient.cpp | 35 ++++++++++++++++++++++---
 1 file changed, 32 insertions(+), 3 deletions(-)

diff --git a/src/platforms/wayland/displayclient.cpp b/src/platforms/wayland/displayclient.cpp
index cbf4f980fb..c94d28b464 100644
--- a/src/platforms/wayland/displayclient.cpp
+++ b/src/platforms/wayland/displayclient.cpp
@@ -28,10 +28,11 @@
 
 #include <boost/throw_exception.hpp>
 
+#include <algorithm>
+#include <condition_variable>
 #include <cstring>
 #include <stdlib.h>
 #include <system_error>
-#include <algorithm>
 
 namespace mgw = mir::graphics::wayland;
 
@@ -94,6 +95,12 @@ class mgw::DisplayClient::Output  :
 
     std::function<void(Output const&)> on_done;
 
+    std::mutex frame_mutex;
+    bool frame_posted = false;
+    std::condition_variable frame_cv;
+
+    void frame_done(struct wl_callback* callback, uint32_t time);
+
     // DisplaySyncGroup implementation
     void for_each_display_buffer(std::function<void(DisplayBuffer&)> const& /*f*/) override;
     void post() override;
@@ -297,8 +304,18 @@ void mgw::DisplayClient::Output::for_each_display_buffer(std::function<void(Disp
     f(*this);
 }
 
+void mgw::DisplayClient::Output::frame_done(struct wl_callback* callback, uint32_t /*time*/) {
+    wl_callback_destroy(callback);
+
+    std::lock_guard<decltype(frame_mutex)> lock{frame_mutex};
+    frame_posted = true;
+    frame_cv.notify_all();
+}
+
 void mgw::DisplayClient::Output::post()
 {
+    std::unique_lock<decltype(frame_mutex)> lock{frame_mutex};
+    frame_cv.wait(lock, [this]{ return frame_posted; });
 }
 
 auto mgw::DisplayClient::Output::recommended_sleep() const -> std::chrono::milliseconds
@@ -343,10 +360,22 @@ void mgw::DisplayClient::Output::release_current()
 void mgw::DisplayClient::Output::swap_buffers()
 {
     // Avoid throttling compositing by blocking in eglSwapBuffers().
-    // NB We can likely do even better by adding a frame callback
-    // and using that to schedule composition
     eglSwapInterval(owner->egldisplay, 0);
 
+    static struct wl_callback_listener const frame_listener =
+        {
+            [](void* data, auto... args)
+                { static_cast<mgw::DisplayClient::Output*>(data)->frame_done(args...); },
+        };
+
+    struct wl_callback *callback = wl_surface_frame(surface);
+    wl_callback_add_listener(callback, &frame_listener, this);
+
+    {
+        std::lock_guard<decltype(frame_mutex)> lock{frame_mutex};
+        frame_posted = false;
+    }
+
     if (eglSwapBuffers(owner->egldisplay, eglsurface) != EGL_TRUE)
         BOOST_THROW_EXCEPTION(egl_error("Failed to perform buffer swap"));
 }
